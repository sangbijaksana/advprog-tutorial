package id.ac.ui.cs.advprog.tutorial3.composite;

import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Ceo;
import id.ac.ui.cs.advprog.tutorial3.composite.higherups.Cto;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.BackendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.FrontendProgrammer;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.NetworkExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.SecurityExpert;
import id.ac.ui.cs.advprog.tutorial3.composite.techexpert.UiUxDesigner;

public class CompositeMain {

    public static void main(String[] args) {
        Company higherups = new Company();
        Company techexpert = new Company();

        Employees ceo = new Ceo("Luffy", 500000.00);
        Employees cto = new Cto("Zorro", 320000.00);
        higherups.addEmployee(ceo);
        higherups.addEmployee(cto);

        Employees backend = new BackendProgrammer("Franky", 94000.00);
        Employees frontend = new FrontendProgrammer("Nami",66000.00);
        Employees uiUxDesigner = new UiUxDesigner("sanji", 177000.00);
        Employees networkExpert = new NetworkExpert("Brook", 83000.00);
        Employees securityExpert = new SecurityExpert("Usop", 100000);
        techexpert.addEmployee(backend);
        techexpert.addEmployee(frontend);
        techexpert.addEmployee(uiUxDesigner);
        techexpert.addEmployee(networkExpert);
        techexpert.addEmployee(securityExpert);

    }
}
