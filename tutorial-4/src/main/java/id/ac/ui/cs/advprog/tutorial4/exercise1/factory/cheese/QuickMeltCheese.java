package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class QuickMeltCheese implements Cheese {

    public String toString() {
        return "Quick Melt Cheese";
    }
}
