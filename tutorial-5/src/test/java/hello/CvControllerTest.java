package hello;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CvController.class)
public class CvControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void cvWithVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", "lala"))
                .andExpect(content().string(containsString("lala, I hope you"
                        + " interested to hire me")))
                .andExpect(content().string(not("blah blha")));
    }

    @Test
    public void cvWithoutVisitor() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not("blah blha")));
    }

    @Test
    public void cvWithNoNameVisitor() throws Exception {
        mockMvc.perform(get("/cv").param("visitor", ""))
                .andExpect(content().string(containsString("This is my CV")))
                .andExpect(content().string(not(", I hope youre interested to hire me")));
    }

    @Test
    public void checkCvFullName() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Aldi Hilman Ramadhani")))
                .andExpect(content().string(not("S. Hawking")));
    }

    @Test
    public void checkCvBirthDate() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("24 Desember 1999")))
                .andExpect(content().string(not("26 Agustus 1998")));
    }

    @Test
    public void checkCvBirthPlace() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Manado")))
                .andExpect(content().string(not("Bangkinang")));
    }

    @Test
    public void checkCvAddress() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("Tangerang Selatan, Banten")))
                .andExpect(content().string(not("Google Mauntain View")));
    }

    @Test
    public void checkCvElementarySchool() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("SDI Al-Azhar BSD")))
                .andExpect(content().string(not("Binus University")));
    }

    @Test
    public void checkCvMiddleSchool() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("SMPI Al-Azhar BSD")))
                .andExpect(content().string(not("Stanford University")));
    }

    @Test
    public void checkCvHighSchool() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("SMA Negeri 2 Tangsel")))
                .andExpect(content().string(not("Hardvard University")));
    }

    @Test
    public void checkCvUniversity() throws Exception {
        mockMvc.perform(get("/cv"))
                .andExpect(content().string(containsString("University of Indonesia")))
                .andExpect(content().string(not("Oxford University")));
    }

    @Test
    public void checkCvEssay() throws Exception {
        String goodEssay = "A sophomore in computer science field who passionate"
                + " in programming and eager to learn new things. Like to Code.";
        String badEssay = "yolo swag";

        mockMvc.perform(get("/cv"))
                .andExpect(content().string(not(goodEssay)))
                .andExpect(content().string(not(badEssay)));
    }

}
